#include "stdafx.h"
#include "Point2D.h"
#include <iostream>
#include <cmath>

Point2D::Point2D(double x1,double y1) : x(x1), y(y1){}

Point2D::Point2D(double x1)
{
	y = 0.0;
	x = x1;
}

Point2D::Point2D()
{

}

Point2D::~Point2D()
{
}

double Point2D::GetX() const
{
	return x;
}

double Point2D::GetY() const
{
	return y;
}
void Point2D::SetXY(double pier, double drug)
{
	pier = x;
	drug = y;
}
const Point2D Point2D:: operator+(const Point2D& v1) const
{
	return Point2D(v1.x + x, v1.y + y);

}
const Point2D Point2D:: operator-(const Point2D& v1) const
{
	return Point2D(x - v1.x, y - v1.y);

}
const Point2D Point2D::operator/(const double & liczba) const
{
	if (liczba == 0)
	{
		std::cout << "Nie dzielimy przez 0!" << std::endl;
		return NULL;
	}
	else if (liczba != 0)
	{
		return Point2D(x/liczba,y/liczba);
	}
}
const Point2D Point2D::operator*(const double & liczba) const
{
	return Point2D(x*liczba, y*liczba);
}

const bool Point2D::operator== (const Point2D & prawy_operand) const
{
	if (x == prawy_operand.x && y == prawy_operand.y)
		return true;
	else
		return false;
}
Point2D::operator double()
{
	double wynik;
	wynik = (x*x) + (y*y);
	wynik = sqrt(wynik);
	return wynik;
}

ostream & operator<<(ostream & out, const Point2D & p)
{
	out << p.x << "," << p.y; 
	return out;
}

istream & operator >> (istream & in, Point2D & p)
{
	
	in >> p.x >> p.y; 
	return in;
} 

/*istream & operator >> (istream & in, Point2D & p)
{
	// TODO: insert return statement here
}*/
