#ifndef SETTINGS_H
#define SETTINGS_H
#include <string>
using namespace std;

class Settings
{
    public:
        virtual ~Settings();
        void UstawPolaczenieZBazaDanych(string);
        void UstawPlikKonfiguracyjny(string);
        static Settings& getInstance();

    protected:

    private:
        Settings();//prywatny konstruktor
        static Settings* set;
        static int counter;
        string connectionString;
        string sciezka;
};

#endif // SETTINGS_H
