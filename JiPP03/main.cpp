#include <iostream>
#include <iomanip>
#include <cstdlib>
#include <time.h>
using namespace std;
int s; //rozmiar macierzy
int **tabA;
int **tabB;
int **tabC;

template<typename A, typename B, typename C>
C mul(A tabA, B tabB, C tabC)
{
	for(int i=0; i<s; i++)
	{
	   for(int j=0; j<s; j++)
	   {
	       tabC[i][j]=0;
	        for(int k=0; k<s; k++)
	        {
		       tabC[i][j] += tabA[i][k] * tabB[k][j];
	   	    }
	   }
   }
}

int main()
{
    srand (time(NULL));
	cout<<"Podaj rozmiar macierzy: ";
	cin>>s;

	tabA = new int* [s];
	for (int i = 0; i < s; ++i)
	tabA[i] = new int [s];

	tabB = new int* [s];
	for (int i = 0; i < s; ++i)
	tabB[i] = new int [s];

	tabC = new int* [s];
	for (int i = 0; i <s; ++i)
	tabC[i] = new int [s];


	for(int i = 0; i < s; i++)
	   for(int j = 0; j < s; j++)
	      tabA[i][j] = rand()%10 +1;
	for(int i = 0; i < s; i++)
	   for(int j = 0; j < s; j++)
	      tabB[i][j] = rand()%10 +1;
	mul(tabA,tabB,tabC);
	cout<<"Wynik mnozenia macierzy to:"<<endl;
	for(int i = 0; i < s; i++)
	{
	   for(int j = 0; j < s; j++)
	      cout<<" "<<tabC[i][j];
	      cout<<endl;
	}
    return 0;
}
