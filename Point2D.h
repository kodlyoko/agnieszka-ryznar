#pragma once
using namespace std;
class Point2D
{
	//wspolrzedne punktu na ktorych wykonujemy operacje
	double x;
	double y;
public:
	Point2D(double x1, double y1);
	Point2D(double x1);
	Point2D();
	~Point2D();
	double GetX() const;
	double GetY() const;
	void SetXY(double, double);

	const Point2D Point2D:: operator+(const Point2D& v1) const;// +
	const Point2D Point2D:: operator-(const Point2D& v1) const;// -
	const Point2D Point2D:: operator/(const double & liczba) const;// /
	const Point2D Point2D:: operator*(const double & liczba) const;// *
// +=
	// -=
// *=
	// /=
	const bool Point2D::operator== (const Point2D & prawy_operand) const;// ==
	operator double();
//	friend istream & operator >> (istream & in, Point2D & p);
//	friend ostream & operator<<(ostream & out, const Point2D & p);

};

