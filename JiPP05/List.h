class HeadNode
{
    HeadNode();
    ~HeadNode()
    {
        delete next;
        delete before;
    }
    HeadNode * next=NULL;
    HeadNode * before=NULL;
    int value;
};

class List
{
    public:
        List();
        ~List(){ delete head; }
        //metody
        bool empty();
        int size();
        int front(List & data);
		int back(List & data);
        void push_front(List & data, int x);
		void push_back(List & data, int x);
		void pop_back(List & data);
		void pop_front(List & data);
		void insert(List * pData);
    private:
        int size;
        HeadNode* head;
        HeadNode* tail;

    //empty size front back push_back pop_back pop_front push_front insert
};


