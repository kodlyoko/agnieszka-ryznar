#ifndef QUEUE_H
#define QUEUE_H

class Node{
int a;
Node* next;
Node* first;
int size;
};
class Queue
{
    public:
        Queue();
        ~Queue();
        bool empty();
        void front();
        void back(Queue & data);
        void pop_front(Queue & data);
        void push_back(Queue & data, int k);
        Node *last;
};

#endif // QUEUE_H
